package com.qty.comassisent;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class CommandEditor extends Activity implements View.OnClickListener {

    private static final String TAG = "CommandEditor";

    public static final String EXTRA_EDIT = "edit";
    public static final String EXTRA_ID = "id";

    private EditText mNameEt;
    private Spinner mSerialPortSpinner;
    private Spinner mBaudRateSpinner;
    private CheckBox mRepeatCb;
    private EditText mCommandsEt;
    private Button mCancelBt;
    private Button mAddAndUpdateBt;
    private CommandDatabase mDatabase;
    private CustomCmd mCommand;
    private ArrayList<String> mDevices;
    private ArrayList<String> mBaudRates;

    private long mId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_command_editor);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setDisplayShowHomeEnabled(false);

        mId = -1;
        mDatabase = CommandDatabase.getInstance(getApplicationContext());

        mNameEt = (EditText) findViewById(R.id.name);
        mSerialPortSpinner = (Spinner) findViewById(R.id.serial_port);
        mBaudRateSpinner = (Spinner) findViewById(R.id.baud_rate);
        mCommandsEt = (EditText) findViewById(R.id.commands);
        mRepeatCb = (CheckBox) findViewById(R.id.repeate);
        mCancelBt = (Button) findViewById(R.id.cancel);
        mAddAndUpdateBt = (Button) findViewById(R.id.add_and_update);

        mCancelBt.setOnClickListener(this);
        mAddAndUpdateBt.setOnClickListener(this);

        SerialPortFinder finder = new SerialPortFinder();
        String[] entryValues = finder.getAllDevicesPath();
        mDevices = new ArrayList<String>();
        for (int i = 0; i < entryValues.length; i++) {
            mDevices.add(entryValues[i]);
        }
        ArrayAdapter<String> devicesAdpater = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, mDevices);
        devicesAdpater.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSerialPortSpinner.setAdapter(devicesAdpater);
        if (devicesAdpater.getCount() > 0) {
            mSerialPortSpinner.setSelection(0);
        }

        mBaudRates = new ArrayList<>();
        String[] rates = getResources().getStringArray(R.array.baudrates_value);
        for (int i = 0; i < rates.length; i++) {
            mBaudRates.add(rates[i]);
        }
        ArrayAdapter<String> baudRateAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, mBaudRates);
        baudRateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mBaudRateSpinner.setAdapter(baudRateAdapter);
        mBaudRateSpinner.setSelection(12);

        onNewIntent(getIntent());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        boolean isEdit = intent.getBooleanExtra(EXTRA_EDIT, false);
        if (isEdit) {
            mId = intent.getLongExtra(EXTRA_ID, -1);
            if (mId > 0) {
                mAddAndUpdateBt.setText(R.string.update);
                mCommand = mDatabase.queryCommandById(mId);
                if (mCommand != null) {
                    mNameEt.setText(mCommand.name);
                    mSerialPortSpinner.setSelection(getDevicePostion(mCommand.serialPort));
                    mBaudRateSpinner.setSelection(getBaudRatePosition(mCommand.baudRate + ""));
                    mRepeatCb.setChecked(mCommand.isRepeat);
                    mCommandsEt.setText(mCommand.originCommands);
                } else {
                    Toast.makeText(this, R.string.not_found_command, Toast.LENGTH_SHORT).show();
                    finish();
                }
            } else {
                Toast.makeText(this, R.string.not_found_command, Toast.LENGTH_SHORT).show();
                finish();
            }
        } else {
            mAddAndUpdateBt.setText(R.string.add);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel:
                finish();
                break;

            case R.id.add_and_update:
                addOrUpdateCommand();
                break;
        }
    }

    private void addOrUpdateCommand() {
        String name = mNameEt.getText().toString().trim();
        String port = (String) mSerialPortSpinner.getSelectedItem();
        String rateStr = (String) mBaudRateSpinner.getSelectedItem();
        boolean repeat = mRepeatCb.isChecked();
        String command = mCommandsEt.getText().toString().trim();
        if (!TextUtils.isEmpty(name)) {
            if (!TextUtils.isEmpty(port)) {
                try {
                    int rate = Integer.parseInt(rateStr);
                    if (!TextUtils.isEmpty(command)) {
                        if (mId > 0) {
                            mCommand.name = name;
                            mCommand.serialPort = port;
                            mCommand.baudRate = rate;
                            mCommand.isRepeat = repeat;
                            mCommand.originCommands = command;
                            int count = mDatabase.updateCommand(mCommand);
                            if (count == 1) {
                                Toast.makeText(this, R.string.update_success, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(this, R.string.update_fail, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            CustomCmd cmd = new CustomCmd(-1, command, name, port, rate, repeat);
                            long position = mDatabase.insertCommand(cmd);
                            if (position > 0) {
                                mId = position;
                                mCommand = mDatabase.queryCommandById(position);
                                mAddAndUpdateBt.setText(R.string.update);
                                Toast.makeText(this, R.string.insert_success, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(this, R.string.insert_fail, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        Toast.makeText(this, R.string.command_not_allow_empty, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "addOrUpdateCommand=>error: ", e);
                    Toast.makeText(this, R.string.illegal_baud_rate, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, R.string.serial_port_not_allow_empty, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, R.string.name_not_allow_empty, Toast.LENGTH_SHORT).show();
        }
    }

    private int getDevicePostion(String port) {
        int position = mDevices.indexOf(port);
        if (position == -1) {
            mDevices.add(port);
            ArrayAdapter<String> devicesAdpater = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, mDevices);
            devicesAdpater.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSerialPortSpinner.setAdapter(devicesAdpater);
        }
        position = mDevices.indexOf(port);
        return position;
    }

    private int getBaudRatePosition(String rate) {
        int position = mBaudRates.indexOf(rate);
        if (position == -1) {
            mBaudRates.add(rate);
            ArrayAdapter<String> baudRateAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, mBaudRates);
            baudRateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mBaudRateSpinner.setAdapter(baudRateAdapter);
        }
        position = mBaudRates.indexOf(rate);
        return position;
    }

}
