package com.qty.comassisent;

import android.app.Notification;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class CommandDatabase {

    private static final String TAG = "CommandDatabase";

    public static final String TABLE_NAME = "commands";
    public static final String _ID = "_id";
    public static final String NAME = "name";
    public static final String PORT = "port";
    public static final String RATE = "rate";
    public static final String REPEAT = "repeat";
    public static final String COMMANDS = "commands";

    private static CommandDatabase mDatabase;

    private ComAssisentSQLiteHelper mHelper;

    public static CommandDatabase getInstance(Context context) {
        if (mDatabase == null) {
            mDatabase = new CommandDatabase(context);
        }
        return mDatabase;
    }

    private CommandDatabase(Context context) {
        mHelper = new ComAssisentSQLiteHelper(context);
    }

    public long insertCommand(CustomCmd cmd) {
        if (cmd == null) {
            Log.e(TAG, "insertCommand=>cmd is null.");
            return -1;
        }
        SQLiteDatabase db = mHelper.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put(NAME, cmd.name);
        value.put(PORT, cmd.serialPort);
        value.put(RATE, cmd.baudRate);
        value.put(REPEAT, cmd.isRepeat ? 1 : 0);
        value.put(COMMANDS, cmd.originCommands);
        return db.insert(TABLE_NAME, null, value);
    }

    public int deleteCommand(CustomCmd cmd) {
        if (cmd == null) {
            Log.e(TAG, "deleteCommand=>cmd is null.");
            return -1;
        }
        if (cmd.getId() > 0) {
            SQLiteDatabase db = mHelper.getWritableDatabase();
            String[] whereArgs = new String[]{cmd.getId() + ""};
            return db.delete(TABLE_NAME, _ID + " = ?", whereArgs);
        } else {
            Log.e(TAG, "deleteCommand=>id : " + cmd.getId() + " not exist.");
        }
        return -1;
    }

    public int deleteCommand(long id) {
        if (id > 0) {
            SQLiteDatabase db = mHelper.getWritableDatabase();
            String[] whereArgs = new String[]{id + ""};
            return db.delete(TABLE_NAME, _ID + " = ?", whereArgs);
        } else {
            Log.e(TAG, "deleteCommand=>Illegal id. id: " + id);
            return -1;
        }
    }

    public int updateCommand(CustomCmd cmd) {
        if (cmd == null) {
            Log.e(TAG, "updateCommand=>cmd is null.");
            return -1;
        }
        SQLiteDatabase db = mHelper.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put(NAME, cmd.name);
        value.put(PORT, cmd.serialPort);
        value.put(RATE, cmd.baudRate);
        value.put(REPEAT, cmd.isRepeat ? 1 : 0);
        value.put(COMMANDS, cmd.originCommands);
        return db.update(TABLE_NAME, value, _ID + " = ?", new String[]{cmd.getId() + ""});
    }

    public CustomCmd queryCommandByName(String name) {
        SQLiteDatabase db = mHelper.getReadableDatabase();
        Cursor c = db.query(TABLE_NAME, null, NAME + " = ?", new String[]{name}, null, null, null);
        if (c != null && c.moveToFirst()) {
            int id = c.getInt(c.getColumnIndex(_ID));
            String n = c.getString(c.getColumnIndex(NAME));
            String port = c.getString(c.getColumnIndex(PORT));
            int rate = c.getInt(c.getColumnIndex(RATE));
            boolean repeat = c.getInt(c.getColumnIndex(REPEAT)) != 0;
            String commands = c.getString(c.getColumnIndex(COMMANDS));
            return new CustomCmd(id, commands, n, port, rate, repeat);
        }
        return null;
    }

    public CustomCmd queryCommandById(long id) {
        SQLiteDatabase db = mHelper.getReadableDatabase();
        Cursor c = db.query(TABLE_NAME, null, _ID + " = ?", new String[]{id + ""}, null, null, null);
        if (c != null && c.moveToFirst()) {
            int _id = c.getInt(c.getColumnIndex(_ID));
            String n = c.getString(c.getColumnIndex(NAME));
            String port = c.getString(c.getColumnIndex(PORT));
            int rate = c.getInt(c.getColumnIndex(RATE));
            boolean repeat = c.getInt(c.getColumnIndex(REPEAT)) != 0;
            String commands = c.getString(c.getColumnIndex(COMMANDS));
            return new CustomCmd(_id, commands, n, port, rate, repeat);
        }
        return null;
    }

    public Cursor getAllCommandsCursor() {
        SQLiteDatabase db = mHelper.getReadableDatabase();
        return db.query(TABLE_NAME, null, null, null,
                null, null, null);
    }

    public ArrayList<CustomCmd> getAllCommands() {
        ArrayList<CustomCmd> commands = new ArrayList<>();
        SQLiteDatabase db = mHelper.getReadableDatabase();
        Cursor c = db.query(TABLE_NAME, null, null, null,
                null, null, null);
        if (c != null) {
            while (c.moveToNext()) {
                int id = c.getInt(c.getColumnIndex(_ID));
                String n = c.getString(c.getColumnIndex(NAME));
                String port = c.getString(c.getColumnIndex(PORT));
                int rate = c.getInt(c.getColumnIndex(RATE));
                boolean repeat = c.getInt(c.getColumnIndex(REPEAT)) != 0;
                String cmds = c.getString(c.getColumnIndex(COMMANDS));
                commands.add(new CustomCmd(id, cmds, n, port, rate, repeat));
            }
        } else {
            Log.e(TAG, "getAllCommands=>cursor is null.");
        }
        return commands;
    }

    private class ComAssisentSQLiteHelper extends SQLiteOpenHelper {

        private static final String DATABASE_NAME = "ComAssisent";
        private static final int DATABASE_VERSION = 1;

        private Context mContext;

        public ComAssisentSQLiteHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            mContext = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ( "
                    + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + NAME
                    + " TEXT NOT NULL, " + PORT + " TEXT NOT NULL, " + RATE
                    + " INTEGER NOT NULL, " + REPEAT + " INTEGER NOT NULL, "
                    + COMMANDS + " TEXT NOT NULL);");
            insertPreDefinedCommands(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        }

        private void insertPreDefinedCommands(SQLiteDatabase db) {
            Resources res = mContext.getResources();
            String[] customCmdsRes = res.getStringArray(R.array.pre_defined_commands);
            for (int i = 0; i < customCmdsRes.length; i++) {
                int id = res.getIdentifier(customCmdsRes[i], "array", mContext.getPackageName());
                try {
                    String[] cmds = res.getStringArray(id);
                    if (cmds.length >= 5) {
                        String name = cmds[0];
                        String port = cmds[1];
                        int rate = Integer.parseInt(cmds[2]);
                        boolean repeat = Boolean.parseBoolean(cmds[3]);
                        String commands = "";
                        for (int j = 4; j < cmds.length; j++) {
                            commands += cmds[j];
                            if (j + 1 < cmds.length) {
                                commands += "\n";
                            }
                        }
                        ContentValues value = new ContentValues();
                        value.put(NAME, name);
                        value.put(PORT, port);
                        value.put(RATE, rate);
                        value.put(REPEAT, repeat ? 1 : 0);
                        value.put(COMMANDS, commands);
                        long position = db.insert(TABLE_NAME, null, value);
                        if (position == -1) {
                            Log.e(TAG, "insertPreDefinedCommands=>" + name + " commands insert error.");
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, "insertPreDefinedCommands=>error: ", e);
                }
            }
        }
    }
}
