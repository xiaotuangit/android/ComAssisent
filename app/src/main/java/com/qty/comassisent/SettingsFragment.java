package com.qty.comassisent;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class SettingsFragment extends PreferenceFragment implements Preference.OnPreferenceChangeListener {

    private static final String TAG = "SettingsFragment";

    private ListPreference mSerialPortCountPref;
    private SwitchPreference mConverHexPref;
    private Preference mRefreshTimePref;
    private SharedPreferences mSharedPreferences;
    private Dialog mRefreshTimeDialog;

    public SettingsFragment() {
        // Required empty public constructor
    }

    public static SettingsFragment newInstance(String param1, String param2) {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.settings);
    }

    @Override
    public void onResume() {
        super.onResume();
        String value = mSharedPreferences.getString(Utils.KEY_SERIAL_PORTS, getString(R.string.default_serial_port_count));
        long refreshDelay = mSharedPreferences.getInt(Utils.KEY_REFRESH_DELAYED_TIME, getResources().getInteger(R.integer.default_refresh_delayed_time));
        boolean conver = mSharedPreferences.getBoolean(Utils.KEY_CONVER_HEX_LOG_TO_STRING, getResources().getBoolean(R.bool.default_conver_hex));
        updateSerialPortsPreferencesSummary(value);
        mRefreshTimePref.setSummary(refreshDelay + " ms");
        mConverHexPref.setChecked(conver);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mSerialPortCountPref = (ListPreference) findPreference(Utils.KEY_SERIAL_PORTS);
        mConverHexPref = (SwitchPreference) findPreference(Utils.KEY_CONVER_HEX_LOG_TO_STRING);
        mRefreshTimePref = findPreference(Utils.KEY_REFRESH_DELAYED_TIME);
        initRefreshTimeDialog();

        mSerialPortCountPref.setOnPreferenceChangeListener(this);
        mConverHexPref.setOnPreferenceChangeListener(this);
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        if (Utils.KEY_REFRESH_DELAYED_TIME.equals(preference.getKey())) {
            if (mRefreshTimeDialog != null) {
                if (!mRefreshTimeDialog.isShowing()) {
                    mRefreshTimeDialog.show();
                }
            }
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if (Utils.KEY_SERIAL_PORTS.equals(preference.getKey())) {
            String value = (String)newValue;
            Log.d(TAG, "onPreferenceChange=>count: " + value);
            updateSerialPortsPreferencesSummary(value);
            return true;
        } else if (Utils.KEY_CONVER_HEX_LOG_TO_STRING.equals(preference.getKey())) {
            mSharedPreferences.edit().putBoolean(Utils.KEY_CONVER_HEX_LOG_TO_STRING, (Boolean)newValue).commit();
            return true;
        }
        return false;
    }

    private void updateSerialPortsPreferencesSummary(String value) {
        CharSequence[] entries = mSerialPortCountPref.getEntries();
        CharSequence[] entryValues = mSerialPortCountPref.getEntryValues();
        for (int i = 0; i < entryValues.length; i++) {
            if (value.equals(entryValues[i])) {
                mSerialPortCountPref.setSummary(entries[i]);
                break;
            }
        }
    }

    private void initRefreshTimeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.setting_interace_refresh_time_title)
                .setView(R.layout.refresh_time)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String time = ((EditText)mRefreshTimeDialog.findViewById(R.id.refresh_time)).getText().toString();
                        try {
                            int delay = Integer.parseInt(time);
                            mSharedPreferences.edit().putInt(Utils.KEY_REFRESH_DELAYED_TIME, delay).apply();
                            mRefreshTimePref.setSummary(delay + " ms");
                        } catch (Exception e) {
                            Log.e(TAG, "onClick=>error: ", e);
                            Toast.makeText(getContext(), R.string.settings_fail, Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        mRefreshTimeDialog = builder.create();
    }
}
