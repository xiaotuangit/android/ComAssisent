package com.qty.comassisent;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.locks.ReadWriteLock;

public class SerialPortHelper {

    private static final String TAG = "SerialPortHelper";

    private String mName;
    private SerialPort mSerialPort;
    private OutputStream mOutputStream;
    private InputStream mInputStream;
    private ReadThread mReadThread;
    private SendThread mSendThread;
    private byte[] mLoopData;
    private OnDataReceive mReceiver;
    private String mPort;
    private int iBaudRate = 9600;
    private boolean isOpen = false;
    private int mRefreshDelay = 50;
    private int mExceDelay = 500;

    public SerialPortHelper(Context context, String name) {
        this(context, name, null);
    }

    public SerialPortHelper(Context context, String name, OnDataReceive receive) {
        this(context, name, "/dev/s3c2410_serial0", 9600, receive);
    }

    public SerialPortHelper(Context context, String name, String sPort, OnDataReceive receive) {
        this(context, name, sPort, 9600, receive);
    }

    public SerialPortHelper(Context context, String name, String sPort, String sBaudRate, OnDataReceive receive) {
        this(context, name, sPort, Integer.parseInt(sBaudRate), receive);
    }

    public SerialPortHelper(Context context, String name, String sPort, int iBaudRate, OnDataReceive receive) {
        this.mName = name;
        this.mPort = sPort;
        this.iBaudRate = iBaudRate;
        this.mReceiver = receive;
        Log.d(TAG, "SerialPortHelper=>name: " + mName + " receive: " + mReceiver);
        Resources res = context.getResources();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        mRefreshDelay = sp.getInt(Utils.KEY_REFRESH_DELAYED_TIME, res.getInteger(R.integer.default_refresh_delayed_time));
    }

    public void open() throws SecurityException, IOException, InvalidParameterException {
        mSerialPort = new SerialPort(new File(mPort), iBaudRate, 0);
        mOutputStream = mSerialPort.getOutputStream();
        mInputStream = mSerialPort.getInputStream();
        mReadThread = new ReadThread();
        mReadThread.start();
        isOpen = true;
    }

    public void close() {
        if (mReadThread != null) {
            mReadThread.stopThread();
            mReadThread.interrupt();
            mReadThread = null;
        }
        if (mSendThread != null) {
            mSendThread.stopThread();
            mSendThread.interrupt();
            mSendThread = null;
        }
        if (mSerialPort != null) {
            mSerialPort.close();
            mSerialPort = null;
        }
        try {
            if (mOutputStream != null) {
                mOutputStream.flush();
                mOutputStream.close();
            }
        } catch (Exception e) {
            Log.e(TAG, "close=>close outputstream error: ", e);
        } finally {
            mOutputStream = null;
        }
        try {
            if (mInputStream != null) {
                mInputStream.close();
            }
        } catch (Exception e) {
            Log.e(TAG, "close=>close inputstream error: ", e);
        } finally {
            mInputStream = null;
        }
        mReceiver = null;
        isOpen = false;
        Log.d(TAG, "close=>name: " + mName + " receive: " + mReceiver);
    }

    private void send(byte[] bOutArray) {
        try {
            if (mOutputStream != null) {
                mOutputStream.write(bOutArray);
            }
        } catch (IOException e) {
            Log.e(TAG, "send=>error: ", e);
        }
    }

    public void sendHex(String sHex) {
        byte[] bOutArray = Utils.HexToByteArr(sHex);
        send(bOutArray);
    }

    public void sendTxt(String sTxt) {
        byte[] bOutArray = sTxt.getBytes();
        send(bOutArray);
    }

    public int getBaudRate() {
        return iBaudRate;
    }

    public boolean setBaudRate(int iBaud) {
        if (isOpen) {
            return false;
        } else {
            iBaudRate = iBaud;
            return true;
        }
    }

    public boolean setBaudRate(String sBaud) {
        int iBaud = Integer.parseInt(sBaud);
        return setBaudRate(iBaud);
    }

    public String getPort() {
        return mPort;
    }

    public boolean setPort(String sPort) {
        if (isOpen) {
            return false;
        } else {
            this.mPort = sPort;
            return true;
        }
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setReceiver(OnDataReceive receiver) {
        this.mReceiver = receiver;
    }

    private byte[] getLoopData() {
        return mLoopData;
    }

    public void setLoopData(byte[] bLoopData) {
        this.mLoopData = bLoopData;
    }

    public void setTxtLoopData(String sTxt) {
        this.mLoopData = sTxt.getBytes();
    }

    public void setHexLoopData(String sHex) {
        this.mLoopData = Utils.HexToByteArr(sHex);
    }

    public void setExceDelay(int time) {
        this.mExceDelay = time;
    }

    public void startSend() {
        if (mSendThread != null) {
            mSendThread.stopThread();
            mSendThread = null;
        }
        mSendThread = new SendThread();
        mSendThread.start();
    }

    public void stopSend() {
        if (mSendThread != null) {
            mSendThread.stopThread();
            mSendThread = null;
        }
    }

    private class ReadThread extends Thread {

        private boolean isStop = false;
        private ByteBuffer datas = ByteBuffer.allocate(4096);

        @Override
        public void run() {
            while (!isStop) {
                try {
                    if (mInputStream == null) return;
                    datas.clear();
                    byte[] buffer = new byte[512];
                    int length = 0;
                    while(mInputStream.available() > 0) {
                        Log.d(TAG, "ReadRun=>name2: " + mName);
                        int size = mInputStream.read(buffer);
                        if (size > 0) {
                            length += size;
                            datas.put(Arrays.copyOfRange(buffer, 0, size));
                        }
                        try {
                            Thread.sleep(Math.round(mRefreshDelay * 0.1));
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    if (mReceiver != null && length > 0) {
                        mReceiver.onDataReceive(datas.array(), length);
                    }
                    try {
                           Thread.sleep(mRefreshDelay);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                    return;
                }
            }
            Log.d(TAG, "ReadRun=>name: " + mName + " Thread end.");
        }

        public void stopThread() {
            isStop = true;
        }
    }

    private class SendThread extends Thread {

        private boolean isStop = false;

        @Override
        public void run() {
            while (!isStop) {
                if (mReceiver != null) {
                    mReceiver.onSendCommand(getLoopData());
                }
                send(getLoopData());
                try {
                    Thread.sleep(mExceDelay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            Log.d(TAG, "SendThread=>name: " + mName + " Thread end.");
        }

        //线程暂停
        private void stopThread() {
            this.isStop = true;
        }
    }

    public interface OnDataReceive {
        void onDataReceive(byte[] data, int length);

        void onSendCommand(byte[] command);
    }
}
