package com.qty.comassisent;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.util.ArrayList;

public class CommandList extends ListActivity implements AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener {

    private static final String TAG = "CommandList";

    private CommandDatabase mDatabase;
    private MyAdapter mAdapter;
    private MenuItem mDeleteItem;
    private MenuItem mAddItem;
    private MenuItem mDoneItem;
    private ArrayList<Long> mSelectItems;

    private boolean isSelectedMode;
    private boolean isDeleting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setDisplayShowHomeEnabled(false);

        isSelectedMode = false;
        isDeleting = false;
        mDatabase = CommandDatabase.getInstance(getApplicationContext());
        mSelectItems = new ArrayList<>();

        getListView().setItemsCanFocus(false);
        getListView().setOnItemClickListener(this);
        getListView().setOnItemLongClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAdapter = new MyAdapter(this, R.layout.command_list_item, mDatabase.getAllCommandsCursor(),
                new String[]{CommandDatabase.NAME}, new int[]{R.id.name},
                SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        getListView().setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.command_list_menu, menu);
        mAddItem = menu.findItem(R.id.add);
        mDeleteItem = menu.findItem(R.id.delete);
        mDoneItem = menu.findItem(R.id.done);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (isSelectedMode) {
            mAddItem.setVisible(false);
            mDeleteItem.setVisible(true);
            mDoneItem.setVisible(true);
        } else {
            mAddItem.setVisible(true);
            mDeleteItem.setVisible(false);
            mDoneItem.setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            case R.id.add:
                Intent editor = new Intent(this, CommandEditor.class);
                editor.putExtra(CommandEditor.EXTRA_EDIT, false);
                startActivity(editor);
                break;

            case R.id.delete:
                isSelectedMode = false;
                mAdapter.notifyDataSetChanged();
                invalidateOptionsMenu();
                int deleteCount = 0;
                for (int i = 0; i < mSelectItems.size(); i++) {
                    int count = mDatabase.deleteCommand(mSelectItems.get(i));
                    deleteCount += count;
                }
                mSelectItems.clear();
                if (deleteCount > 0) {
                    mAdapter = new MyAdapter(this, R.layout.command_list_item, mDatabase.getAllCommandsCursor(),
                            new String[]{CommandDatabase.NAME}, new int[]{R.id.name},
                            SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
                    getListView().setAdapter(mAdapter);
                }
                Toast.makeText(this, getResources().getQuantityString(R.plurals.delete_counts,
                        deleteCount, deleteCount), Toast.LENGTH_SHORT).show();
                break;

            case R.id.done:
                isSelectedMode = false;
                mSelectItems.clear();
                mAdapter.notifyDataSetChanged();
                invalidateOptionsMenu();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        if (!isSelectedMode) {
            mSelectItems.clear();
            isSelectedMode = true;
            mAdapter.notifyDataSetChanged();
            invalidateOptionsMenu();
            mSelectItems.add(id);
            CheckBox cb = (CheckBox) view.findViewById(android.R.id.checkbox);
            cb.setChecked(true);
            return true;
        }
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d(TAG, "onItemClick=>position: " + position + " id: " + id);
        if (isSelectedMode) {
            CheckBox cb = (CheckBox) view.findViewById(android.R.id.checkbox);
            if (mSelectItems.contains(id)) {
                mSelectItems.remove(id);
                cb.setChecked(false);
            } else {
                cb.setChecked(true);
                mSelectItems.add(id);
            }
        } else {
            Intent editor = new Intent(this, CommandEditor.class);
            editor.putExtra(CommandEditor.EXTRA_EDIT, true);
            editor.putExtra(CommandEditor.EXTRA_ID, id);
            startActivity(editor);
        }
    }

    private class MyAdapter extends SimpleCursorAdapter {

        public MyAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
            super(context, layout, c, from, to, flags);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            super.bindView(view, context, cursor);
            CheckBox cb = (CheckBox) view.findViewById(android.R.id.checkbox);
            if (isSelectedMode) {
                cb.setVisibility(View.VISIBLE);
            } else {
                cb.setVisibility(View.GONE);
            }
        }
    }
}
