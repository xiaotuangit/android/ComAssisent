package com.qty.comassisent;

import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;

public class CustomCmd {

    private static final String TAG = "CustomCmd";

    public ArrayList<Command> commands;
    public String name;
    public String serialPort;
    public String originCommands;
    public int baudRate;
    private int id;
    public boolean isRepeat;

    public CustomCmd(int id, String commands, String name, String port, int rate, boolean repeat) {
        this.id = id;
        this.name = name;
        this.serialPort = port;
        this.baudRate = rate;
        this.originCommands = commands;
        this.isRepeat = repeat;
        this.commands = parserOriginCommands();
    }

    public int getId() {
        return id;
    }

    private ArrayList<Command> parserOriginCommands() {
        ArrayList<Command>  cmds = new ArrayList<>();
        if (!TextUtils.isEmpty(originCommands)) {
            String[] lines = originCommands.split("\n");
            for (int i = 0; i < lines.length; i++) {
                Command cmd = Command.parserCommand(lines[i]);
                if (cmd != null) {
                    cmds.add(cmd);
                }
            }
        }

        return cmds;
    }
}
