package com.qty.comassisent;

import android.util.Log;

public class Command {

    private static final String TAG = "Command";

    public String cmd;
    public int exceTime;
    public boolean isSendHex;
    public boolean isAddLineBreak;

    public Command(String cmd, int exceTime, boolean sendHex, boolean addLineBreak) {
        this.cmd = cmd;
        this.exceTime = exceTime;
        this.isSendHex = sendHex;
        this.isAddLineBreak = addLineBreak;
    }

    public static Command parserCommand(String cmd) {
        String[] items = cmd.split("\\|");
        Log.d(TAG, "parserCommand=>length: " + items.length);
        if (items.length == 4) {
            try {
                String command = items[0];
                boolean hex = Boolean.parseBoolean(items[1]);
                boolean line = Boolean.parseBoolean(items[2]);
                int time = Integer.parseInt(items[3]);
                return new Command(command, time, hex, line);
            } catch (Exception e) {
                Log.e(TAG, "parserCommand=>parser: " + cmd + " error: ", e);
            }
        }
        return null;
    }
}
