package com.qty.comassisent;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ComAssisent extends Activity {

    private static final String TAG = "ComAssisent";
    private static final boolean DEBUG = true;

    private EditText mLogEt;
    private Button mSettingsBt;
    private Button mClearBt;
    private CheckBox mAutoClearCb;
    private EditText mMaxLineEt;
    private Spinner mCommandsSpinner;
    private Button mCommandsSendBt;
    private Button mSaveLogBt;

    private View mComABContainer;
    private View mComCDContainer;

    // COMA
    private View mComAContainer;
    private EditText mComACommandEt;
    private Spinner mComASerialPortSpinner;
    private Spinner mComABaudRateSpinner;
    private ToggleButton mComAToggleTb;
    private RadioButton mComATxtRb;
    private RadioButton mComAHexRb;
    private CheckBox mComALineBreakCb;
    private EditText mComAAutoSendDelayedTimeEt;
    private CheckBox mComAAutoSendCb;
    private Button mComASendBt;

    // COMB
    private View mComBContainer;
    private EditText mComBCommandEt;
    private Spinner mComBSerialPortSpinner;
    private Spinner mComBBaudRateSpinner;
    private ToggleButton mComBToggleTb;
    private RadioButton mComBTxtRb;
    private RadioButton mComBHexRb;
    private CheckBox mComBLineBreakCb;
    private EditText mComBAutoSendDelayedTimeEt;
    private CheckBox mComBAutoSendCb;
    private Button mComBSendBt;

    // COMC
    private View mComCContainer;
    private EditText mComCCommandEt;
    private Spinner mComCSerialPortSpinner;
    private Spinner mComCBaudRateSpinner;
    private ToggleButton mComCToggleTb;
    private RadioButton mComCTxtRb;
    private RadioButton mComCHexRb;
    private CheckBox mComCLineBreakCb;
    private EditText mComCAutoSendDelayedTimeEt;
    private CheckBox mComCAutoSendCb;
    private Button mComCSendBt;

    // COMD
    private View mComDContainer;
    private EditText mComDCommandEt;
    private Spinner mComDSerialPortSpinner;
    private Spinner mComDBaudRateSpinner;
    private ToggleButton mComDToggleTb;
    private RadioButton mComDTxtRb;
    private RadioButton mComDHexRb;
    private CheckBox mComDLineBreakCb;
    private EditText mComDAutoSendDelayedTimeEt;
    private CheckBox mComDAutoSendCb;
    private Button mComDSendBt;

    private SerialPortHelper mComAHelper;
    private SerialPortHelper mComBHelper;
    private SerialPortHelper mComCHelper;
    private SerialPortHelper mComDHelper;
    private SharedPreferences mSharedPreferences;
    private ArrayList<CustomCmd> mCustomCmds;
    private DispQueueThread mDispThread;
    private SendSequenceCommandThread mCustomSendThread;
    private CommandDatabase mDatabase;
    private Dialog mEditFileNameDialog;
    private EditText mFileNameEt;
    private SimpleDateFormat mSimpleDateFormat;

    private String mComAIncompleteLog;
    private String mComBIncompleteLog;
    private String mComCIncompleteLog;
    private String mComDIncompleteLog;
    private String mCustomIncompleteLog;
    private int mSerialPorts;
    private int mLines;
    private int mMaxLine;
    private int mComAAutoSendDelayedTime;
    private int mComBAutoSendDelayedTime;
    private int mComCAutoSendDelayedTime;
    private int mComDAutoSendDelayedTime;
    private boolean isAutoClear;
    private boolean isComALineBreak;
    private boolean isComAAutoSend;
    private boolean isComASendHex;
    private boolean isComBLineBreak;
    private boolean isComBAutoSend;
    private boolean isComBSendHex;
    private boolean isComCLineBreak;
    private boolean isComCAutoSend;
    private boolean isComCSendHex;
    private boolean isComDLineBreak;
    private boolean isComDAutoSend;
    private boolean isComDSendHex;
    private boolean isConverHexLogToString;
    private boolean isSendingCommad;
    private boolean isSendingCustomCommand;
    private boolean isComASendingCommand;
    private boolean isComBSendingCommand;
    private boolean isComCSendingCommand;
    private boolean isComDSendingCommand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.com_assisent);

        Resources res = getResources();
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mSerialPorts = Integer.parseInt(mSharedPreferences.getString(Utils.KEY_SERIAL_PORTS,
                String.valueOf(res.getInteger(R.integer.default_serial_ports))));
        isConverHexLogToString = mSharedPreferences.getBoolean(Utils.KEY_CONVER_HEX_LOG_TO_STRING, res.getBoolean(R.bool.default_hex_log_to_string));
        mLines = 0;
        mMaxLine = res.getInteger(R.integer.default_max_line);
        mComAAutoSendDelayedTime = res.getInteger(R.integer.default_com_a_auto_send_delayed_time);
        mComBAutoSendDelayedTime = res.getInteger(R.integer.default_com_b_auto_send_delayed_time);
        mComCAutoSendDelayedTime = res.getInteger(R.integer.default_com_c_auto_send_delayed_time);
        mComDAutoSendDelayedTime = res.getInteger(R.integer.default_com_d_auto_send_delayed_time);
        isAutoClear = res.getBoolean(R.bool.default_auto_clear);
        isComALineBreak = res.getBoolean(R.bool.default_com_a_auto_line_break);
        isComAAutoSend = res.getBoolean(R.bool.default_com_a_auto_send);
        isComASendHex = res.getBoolean(R.bool.default_com_a_send_hex);
        isComBLineBreak = res.getBoolean(R.bool.default_com_b_auto_line_break);
        isComBAutoSend = res.getBoolean(R.bool.default_com_b_auto_send);
        isComBSendHex = res.getBoolean(R.bool.default_com_b_send_hex);
        isComCLineBreak = res.getBoolean(R.bool.default_com_c_auto_line_break);
        isComCAutoSend = res.getBoolean(R.bool.default_com_c_auto_send);
        isComCSendHex = res.getBoolean(R.bool.default_com_c_send_hex);
        isComDLineBreak = res.getBoolean(R.bool.default_com_d_auto_line_break);
        isComDAutoSend = res.getBoolean(R.bool.default_com_d_auto_send);
        isComDSendHex = res.getBoolean(R.bool.default_com_d_send_hex);
        isSendingCommad = false;
        isSendingCustomCommand = false;
        isComASendingCommand = false;
        isComBSendingCommand = false;
        isComCSendingCommand = false;
        isComDSendingCommand = false;
        mComAIncompleteLog = "";
        mComBIncompleteLog = "";
        mComCIncompleteLog = "";
        mComDIncompleteLog = "";
        mCustomIncompleteLog = "";

        mDispThread = new DispQueueThread();
        mDispThread.start();
        mDatabase = CommandDatabase.getInstance(getApplicationContext());
        mSimpleDateFormat = new SimpleDateFormat("yyyyMMddhhmmssS");

        initViews();

        updateSerialPortWindows();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateCustomCommandSend();
        isConverHexLogToString = mSharedPreferences.getBoolean(Utils.KEY_CONVER_HEX_LOG_TO_STRING, getResources().getBoolean(R.bool.default_conver_hex));
        int ports = Integer.parseInt(mSharedPreferences.getString(Utils.KEY_SERIAL_PORTS,
                String.valueOf(getResources().getInteger(R.integer.default_serial_ports))));
        if (ports != mSerialPorts) {
            mSerialPorts = ports;
            updateSerialPortWindows();
        }
        updateUI();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopAllSerialPorts();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDispThread != null) {
            mDispThread.stopThread();
        }
    }

    private void initViews() {
        mLogEt = (EditText) findViewById(R.id.log);
        mSettingsBt = (Button) findViewById(R.id.settings);
        mClearBt = (Button) findViewById(R.id.clear);
        mAutoClearCb = (CheckBox) findViewById(R.id.auto_clear);
        mMaxLineEt = (EditText) findViewById(R.id.max_line);
        mCommandsSpinner = (Spinner) findViewById(R.id.custom_commands);
        mCommandsSendBt = (Button) findViewById(R.id.custom_send);
        mSaveLogBt = (Button) findViewById(R.id.save_log);

        mComABContainer = findViewById(R.id.ab_com_container);
        mComCDContainer = findViewById(R.id.cd_com_container);

        // COMA
        mComAContainer = findViewById(R.id.com_a_container);
        mComACommandEt = (EditText) findViewById(R.id.com_a_command);
        mComASerialPortSpinner = (Spinner) findViewById(R.id.com_a_port);
        mComABaudRateSpinner = (Spinner) findViewById(R.id.com_a_baud_rade);
        mComAToggleTb = (ToggleButton) findViewById(R.id.com_a_toggle);
        mComATxtRb = (RadioButton) findViewById(R.id.com_a_txt);
        mComAHexRb = (RadioButton) findViewById(R.id.com_a_hex);
        mComALineBreakCb = (CheckBox) findViewById(R.id.com_a_line_break);
        mComAAutoSendDelayedTimeEt = (EditText) findViewById(R.id.com_a_send_delay);
        mComAAutoSendCb = (CheckBox) findViewById(R.id.com_a_auto_send);
        mComASendBt = (Button) findViewById(R.id.com_a_send);

        // COMB
        mComBContainer = findViewById(R.id.com_b_container);
        mComBCommandEt = (EditText) findViewById(R.id.com_b_command);
        mComBSerialPortSpinner = (Spinner) findViewById(R.id.com_b_port);
        mComBBaudRateSpinner = (Spinner) findViewById(R.id.com_b_baud_rade);
        mComBToggleTb = (ToggleButton) findViewById(R.id.com_b_toggle);
        mComBTxtRb = (RadioButton) findViewById(R.id.com_b_txt);
        mComBHexRb = (RadioButton) findViewById(R.id.com_b_hex);
        mComBLineBreakCb = (CheckBox) findViewById(R.id.com_b_line_break);
        mComBAutoSendDelayedTimeEt = (EditText) findViewById(R.id.com_b_send_delay);
        mComBAutoSendCb = (CheckBox) findViewById(R.id.com_b_auto_send);
        mComBSendBt = (Button) findViewById(R.id.com_b_send);

        // COMC
        mComCContainer = findViewById(R.id.com_c_container);
        mComCCommandEt = (EditText) findViewById(R.id.com_c_command);
        mComCSerialPortSpinner = (Spinner) findViewById(R.id.com_c_port);
        mComCBaudRateSpinner = (Spinner) findViewById(R.id.com_c_baud_rade);
        mComCToggleTb = (ToggleButton) findViewById(R.id.com_c_toggle);
        mComCTxtRb = (RadioButton) findViewById(R.id.com_c_txt);
        mComCHexRb = (RadioButton) findViewById(R.id.com_c_hex);
        mComCLineBreakCb = (CheckBox) findViewById(R.id.com_c_line_break);
        mComCAutoSendDelayedTimeEt = (EditText) findViewById(R.id.com_c_send_delay);
        mComCAutoSendCb = (CheckBox) findViewById(R.id.com_c_auto_send);
        mComCSendBt = (Button) findViewById(R.id.com_c_send);

        // COMD
        mComDContainer = findViewById(R.id.com_d_container);
        mComDCommandEt = (EditText) findViewById(R.id.com_d_command);
        mComDSerialPortSpinner = (Spinner) findViewById(R.id.com_d_port);
        mComDBaudRateSpinner = (Spinner) findViewById(R.id.com_d_baud_rade);
        mComDToggleTb = (ToggleButton) findViewById(R.id.com_d_toggle);
        mComDTxtRb = (RadioButton) findViewById(R.id.com_d_txt);
        mComDHexRb = (RadioButton) findViewById(R.id.com_d_hex);
        mComDLineBreakCb = (CheckBox) findViewById(R.id.com_d_line_break);
        mComDAutoSendDelayedTimeEt = (EditText) findViewById(R.id.com_d_send_delay);
        mComDAutoSendCb = (CheckBox) findViewById(R.id.com_d_auto_send);
        mComDSendBt = (Button) findViewById(R.id.com_d_send);

        mSettingsBt.setOnClickListener(mButtonClickListener);
        mClearBt.setOnClickListener(mButtonClickListener);
        mCommandsSendBt.setOnClickListener(mButtonClickListener);
        mSaveLogBt.setOnClickListener(mButtonClickListener);
        mComASendBt.setOnClickListener(mButtonClickListener);
        mComBSendBt.setOnClickListener(mButtonClickListener);
        mComCSendBt.setOnClickListener(mButtonClickListener);
        mComDSendBt.setOnClickListener(mButtonClickListener);

        mAutoClearCb.setOnCheckedChangeListener(mCheckBoxCheckedChangeListener);
        mComALineBreakCb.setOnCheckedChangeListener(mCheckBoxCheckedChangeListener);
        mComAAutoSendCb.setOnCheckedChangeListener(mCheckBoxCheckedChangeListener);
        mComBLineBreakCb.setOnCheckedChangeListener(mCheckBoxCheckedChangeListener);
        mComBAutoSendCb.setOnCheckedChangeListener(mCheckBoxCheckedChangeListener);
        mComCLineBreakCb.setOnCheckedChangeListener(mCheckBoxCheckedChangeListener);
        mComCAutoSendCb.setOnCheckedChangeListener(mCheckBoxCheckedChangeListener);
        mComDLineBreakCb.setOnCheckedChangeListener(mCheckBoxCheckedChangeListener);
        mComDAutoSendCb.setOnCheckedChangeListener(mCheckBoxCheckedChangeListener);

        mComATxtRb.setOnCheckedChangeListener(mRadioButtonCheckedChangeListener);
        mComAHexRb.setOnCheckedChangeListener(mRadioButtonCheckedChangeListener);
        mComBTxtRb.setOnCheckedChangeListener(mRadioButtonCheckedChangeListener);
        mComBHexRb.setOnCheckedChangeListener(mRadioButtonCheckedChangeListener);
        mComCTxtRb.setOnCheckedChangeListener(mRadioButtonCheckedChangeListener);
        mComCHexRb.setOnCheckedChangeListener(mRadioButtonCheckedChangeListener);
        mComDTxtRb.setOnCheckedChangeListener(mRadioButtonCheckedChangeListener);
        mComDHexRb.setOnCheckedChangeListener(mRadioButtonCheckedChangeListener);

        mComAToggleTb.setOnCheckedChangeListener(mToggleButtonCheckedChangeListener);
        mComBToggleTb.setOnCheckedChangeListener(mToggleButtonCheckedChangeListener);
        mComCToggleTb.setOnCheckedChangeListener(mToggleButtonCheckedChangeListener);
        mComDToggleTb.setOnCheckedChangeListener(mToggleButtonCheckedChangeListener);

        mMaxLineEt.setText(mMaxLine + "");
        mAutoClearCb.setChecked(isAutoClear);
        mMaxLineEt.setEnabled(!isAutoClear);

        SerialPortFinder finder = new SerialPortFinder();
        String[] entryValues = finder.getAllDevicesPath();
        List<String> allDevices = new ArrayList<String>();
        for (int i = 0; i < entryValues.length; i++) {
            allDevices.add(entryValues[i]);
        }
        ArrayAdapter<String> devicesAdpater = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, allDevices);
        devicesAdpater.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mComASerialPortSpinner.setAdapter(devicesAdpater);
        mComBSerialPortSpinner.setAdapter(devicesAdpater);
        mComCSerialPortSpinner.setAdapter(devicesAdpater);
        mComDSerialPortSpinner.setAdapter(devicesAdpater);
        if (devicesAdpater.getCount() > 0) {
            mComASerialPortSpinner.setSelection(0 % devicesAdpater.getCount());
            mComBSerialPortSpinner.setSelection(1 % devicesAdpater.getCount());
            mComCSerialPortSpinner.setSelection(2 % devicesAdpater.getCount());
            mComDSerialPortSpinner.setSelection(3 % devicesAdpater.getCount());
        } else {
            Log.e(TAG, "initViews=>devices not found.");
        }

        ArrayAdapter<CharSequence> baudRateAdapter = ArrayAdapter.createFromResource(this,
                R.array.baudrates_value, android.R.layout.simple_spinner_item);
        baudRateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mComABaudRateSpinner.setAdapter(baudRateAdapter);
        mComABaudRateSpinner.setSelection(12);
        mComBBaudRateSpinner.setAdapter(baudRateAdapter);
        mComBBaudRateSpinner.setSelection(12);
        mComCBaudRateSpinner.setAdapter(baudRateAdapter);
        mComCBaudRateSpinner.setSelection(12);
        mComDBaudRateSpinner.setAdapter(baudRateAdapter);
        mComDBaudRateSpinner.setSelection(12);

        mComATxtRb.setChecked(!isComASendHex);
        mComAHexRb.setChecked(isComASendHex);
        mComBTxtRb.setChecked(!isComBSendHex);
        mComBHexRb.setChecked(isComBSendHex);
        mComCTxtRb.setChecked(!isComCSendHex);
        mComCHexRb.setChecked(isComCSendHex);
        mComDTxtRb.setChecked(!isComDSendHex);
        mComDHexRb.setChecked(isComDSendHex);

        mComALineBreakCb.setChecked(isComALineBreak);
        mComBLineBreakCb.setChecked(isComBLineBreak);
        mComCLineBreakCb.setChecked(isComCLineBreak);
        mComDLineBreakCb.setChecked(isComDLineBreak);

        mComAAutoSendCb.setChecked(isComAAutoSend);
        mComBAutoSendCb.setChecked(isComBAutoSend);
        mComCAutoSendCb.setChecked(isComCAutoSend);
        mComDAutoSendCb.setChecked(isComDAutoSend);

        mComAAutoSendDelayedTimeEt.setText(mComAAutoSendDelayedTime + "");
        mComAAutoSendDelayedTimeEt.setEnabled(!isComAAutoSend);
        mComBAutoSendDelayedTimeEt.setText(mComBAutoSendDelayedTime + "");
        mComBAutoSendDelayedTimeEt.setEnabled(!isComBAutoSend);
        mComCAutoSendDelayedTimeEt.setText(mComCAutoSendDelayedTime + "");
        mComCAutoSendDelayedTimeEt.setEnabled(!isComCAutoSend);
        mComDAutoSendDelayedTimeEt.setText(mComDAutoSendDelayedTime + "");
        mComDAutoSendDelayedTimeEt.setEnabled(!isComDAutoSend);
    }

    private void initEditFileNameDialog() {
        View view = getLayoutInflater().inflate(R.layout.edit_log_file_name, null);
        mFileNameEt = (EditText) view.findViewById(R.id.file_name);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.edit_log_file_name);
        builder.setView(view);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (mFileNameEt != null) {
                    String fileName = mFileNameEt.getText().toString();
                    if (TextUtils.isEmpty(fileName)) {
                        fileName = mFileNameEt.getHint().toString();
                    }
                    Log.d(TAG, "onClick=>file name: " + fileName);
                    if (saveLogToFile(fileName)) {
                        Toast.makeText(ComAssisent.this, R.string.save_log_success, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ComAssisent.this, R.string.save_log_fail, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        mEditFileNameDialog = builder.create();
    }

    private void updateUI() {
        updateLogUI();
        updateComAUI();
        updateComBUI();
        updateComCUI();
        updateComDUI();
    }

    private void updateLogUI() {
        mSettingsBt.setEnabled(!isSendingCommad);
        mCommandsSpinner.setEnabled(!isSendingCustomCommand);
        mAutoClearCb.setChecked(isAutoClear);
        mMaxLineEt.setEnabled(!isAutoClear);
        mCommandsSendBt.setText(isSendingCustomCommand ? R.string.stop_title : R.string.send_title);
    }

    private void updateComAUI() {
        boolean isOpened = mComAHelper != null && mComAHelper.isOpen();
        mComAToggleTb.setChecked(isOpened);
        mComACommandEt.setEnabled(!isComASendingCommand);
        mComASerialPortSpinner.setEnabled(!isOpened);
        mComABaudRateSpinner.setEnabled(!isOpened);
        mComATxtRb.setEnabled(!isComASendingCommand);
        mComAHexRb.setEnabled(!isComASendingCommand);
        mComALineBreakCb.setEnabled(!isComASendingCommand);
        mComAAutoSendDelayedTimeEt.setEnabled(!isComASendingCommand && !isComAAutoSend);
        mComAAutoSendCb.setEnabled(!isComASendingCommand);
        if (isComAAutoSend) {
            mComASendBt.setText(isComASendingCommand ? R.string.stop_title : R.string.send_title);
            mComASendBt.setEnabled(isOpened);
        } else {
            mComASendBt.setText(R.string.send_title);
            mComASendBt.setEnabled(!isComASendingCommand && isOpened);
        }
    }

    private void updateComBUI() {
        boolean isOpened = mComBHelper != null && mComBHelper.isOpen();
        mComBToggleTb.setChecked(isOpened);
        mComBCommandEt.setEnabled(!isComBSendingCommand);
        mComBSerialPortSpinner.setEnabled(!isOpened);
        mComBBaudRateSpinner.setEnabled(!isOpened);
        mComBTxtRb.setEnabled(!isComBSendingCommand);
        mComBHexRb.setEnabled(!isComBSendingCommand);
        mComBLineBreakCb.setEnabled(!isComBSendingCommand);
        mComBAutoSendDelayedTimeEt.setEnabled(!isComBSendingCommand && !isComBAutoSend);
        mComBAutoSendCb.setEnabled(!isComBSendingCommand);
        if (isComBAutoSend) {
            mComBSendBt.setText(isComBSendingCommand ? R.string.stop_title : R.string.send_title);
            mComBSendBt.setEnabled(isOpened);
        } else {
            mComBSendBt.setText(R.string.send_title);
            mComBSendBt.setEnabled(!isComBSendingCommand && isOpened);
        }
    }

    private void updateComCUI() {
        boolean isOpened = mComCHelper != null && mComCHelper.isOpen();
        mComCToggleTb.setChecked(isOpened);
        mComCCommandEt.setEnabled(!isComCSendingCommand);
        mComCSerialPortSpinner.setEnabled(!isOpened);
        mComCBaudRateSpinner.setEnabled(!isOpened);
        mComCTxtRb.setEnabled(!isComCSendingCommand);
        mComCHexRb.setEnabled(!isComCSendingCommand);
        mComCLineBreakCb.setEnabled(!isComCSendingCommand);
        mComCAutoSendDelayedTimeEt.setEnabled(!isComCSendingCommand && !isComCAutoSend);
        mComCAutoSendCb.setEnabled(!isComCSendingCommand);
        if (isComCAutoSend) {
            mComCSendBt.setText(isComCSendingCommand ? R.string.stop_title : R.string.send_title);
            mComCSendBt.setEnabled(isOpened);
        } else {
            mComCSendBt.setText(R.string.send_title);
            mComCSendBt.setEnabled(!isComCSendingCommand && isOpened);
        }
    }

    private void updateComDUI() {
        boolean isOpened = mComDHelper != null && mComDHelper.isOpen();
        mComDToggleTb.setChecked(isOpened);
        mComDCommandEt.setEnabled(!isComDSendingCommand);
        mComDSerialPortSpinner.setEnabled(!isOpened);
        mComDBaudRateSpinner.setEnabled(!isOpened);
        mComDTxtRb.setEnabled(!isComDSendingCommand);
        mComDHexRb.setEnabled(!isComDSendingCommand);
        mComDLineBreakCb.setEnabled(!isComDSendingCommand);
        mComDAutoSendDelayedTimeEt.setEnabled(!isComDSendingCommand && !isComDAutoSend);
        mComDAutoSendCb.setEnabled(!isComDSendingCommand);
        if (isComDAutoSend) {
            mComDSendBt.setText(isComDSendingCommand ? R.string.stop_title : R.string.send_title);
            mComDSendBt.setEnabled(isOpened);
        } else {
            mComDSendBt.setText(R.string.send_title);
            mComDSendBt.setEnabled(!isComDSendingCommand && isOpened);
        }
    }

    private void updateCustomCommandSend() {
        mCustomCmds = mDatabase.getAllCommands();

        List<String> cmdNames = new ArrayList<String>();
        for (int i = 0; i < mCustomCmds.size(); i++) {
            cmdNames.add(mCustomCmds.get(i).name);
        }
        ArrayAdapter<String> customCmdAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, cmdNames);
        customCmdAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mCommandsSpinner.setAdapter(customCmdAdapter);
        if (customCmdAdapter.getCount() > 0) {
            mCommandsSpinner.setSelection(0);
            mCommandsSendBt.setEnabled(true);
        } else {
            mCommandsSendBt.setEnabled(false);
        }
    }

    private void stopAllSerialPorts() {
        if (mComAHelper != null) {
            mComAHelper.close();
            mComAHelper = null;
        }
        if (mComBHelper != null) {
            mComBHelper.close();
            mComBHelper = null;
        }
        if (mComCHelper != null) {
            mComCHelper.close();
            mComCHelper = null;
        }
        if (mComDHelper != null) {
            mComDHelper.close();
            mComDHelper = null;
        }
    }

    private void updateSerialPortWindows() {
        switch (mSerialPorts) {
            case 1:
                mComCDContainer.setVisibility(View.GONE);
                mComABContainer.setVisibility(View.VISIBLE);
                mComBContainer.setVisibility(View.GONE);
                mComAContainer.setVisibility(View.VISIBLE);
                break;

            case 2:
                mComCDContainer.setVisibility(View.GONE);
                mComABContainer.setVisibility(View.VISIBLE);
                mComBContainer.setVisibility(View.VISIBLE);
                mComAContainer.setVisibility(View.VISIBLE);
                break;

            case 3:
                mComCDContainer.setVisibility(View.VISIBLE);
                mComABContainer.setVisibility(View.VISIBLE);
                mComBContainer.setVisibility(View.GONE);
                mComAContainer.setVisibility(View.VISIBLE);
                mComCContainer.setVisibility(View.VISIBLE);
                mComDContainer.setVisibility(View.VISIBLE);
                break;

            case 4:
                mComCDContainer.setVisibility(View.VISIBLE);
                mComABContainer.setVisibility(View.VISIBLE);
                mComBContainer.setVisibility(View.VISIBLE);
                mComAContainer.setVisibility(View.VISIBLE);
                mComCContainer.setVisibility(View.VISIBLE);
                mComDContainer.setVisibility(View.VISIBLE);
                break;
        }
    }

    private SerialPortHelper getSerialPortOpen(String port) {
        if (mComAHelper != null && mComAHelper.isOpen()) {
            if (port.equals(mComASerialPortSpinner.getSelectedItem())) {
                return mComAHelper;
            }
        }
        if (mComBHelper != null && mComBHelper.isOpen()) {
            if (port.equals(mComBSerialPortSpinner.getSelectedItem())) {
                return mComBHelper;
            }
        }
        if (mComCHelper != null && mComCHelper.isOpen()) {
            if (port.equals(mComCSerialPortSpinner.getSelectedItem())) {
                return mComCHelper;
            }
        }
        if (mComDHelper != null && mComDHelper.isOpen()) {
            if (port.equals(mComDSerialPortSpinner.getSelectedItem())) {
                return mComDHelper;
            }
        }
        if (mCustomSendThread != null && mCustomSendThread.getCustomHelper() != null) {
            if (port.equals(mCustomSendThread.mSerialPort)) {
                return mCustomSendThread.mCustomHelper;
            }
        }
        return null;
    }

    private void comASendCommand() {
        if (mComAHelper.isOpen()) {
            String cmd = mComACommandEt.getText().toString();
            if (!TextUtils.isEmpty(cmd)) {
                if (isComAAutoSend) {
                    if (isComASendHex) {
                        if (!Utils.isHexString(cmd)) {
                            Toast.makeText(this, R.string.invalid_hex_string, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (isComALineBreak) {
                            mComAHelper.setHexLoopData(cmd + String.valueOf((int) ('\r')) + String.valueOf((int) ('\n')));
                        } else {
                            mComAHelper.setHexLoopData(cmd);
                        }
                    } else {
                        if (isComALineBreak) {
                            mComAHelper.setTxtLoopData(cmd + "\r\n");
                        } else {
                            mComAHelper.setTxtLoopData(cmd);
                        }
                    }
                    try {
                        int delayTime = Integer.parseInt(mComAAutoSendDelayedTimeEt.getText().toString());
                        mComAHelper.setExceDelay(delayTime);
                    } catch (Exception e) {
                        Log.e(TAG, "comASendCommand=>error: ", e);
                    }
                    mComAHelper.startSend();
                } else {
                    if (isComASendHex) {
                        if (!Utils.isHexString(cmd)) {
                            Toast.makeText(this, R.string.invalid_hex_string, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (isComALineBreak) {
                            mComAHelper.sendHex(cmd + String.valueOf((int) ('\r')) + String.valueOf((int) ('\n')));
                        } else {
                            mComAHelper.sendHex(cmd);
                        }
                    } else {
                        if (isComALineBreak) {
                            mComAHelper.sendTxt(cmd + "\r\n");
                        } else {
                            mComAHelper.sendTxt(cmd);
                        }
                    }
                    if (mSerialPorts > 1) {
                        cmd = String.format("%6s[%-7s]: %s", "[COMA]", getString(R.string.send_tag), cmd);
                    } else {
                        cmd = String.format("[%-7s]: %s", getString(R.string.send_tag), cmd);
                    }
                    mDispThread.addLogToQueue(cmd);
                }
            } else {
                Toast.makeText(this, R.string.command_not_allow_empty, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void comBSendCommand() {
        if (mComBHelper.isOpen()) {
            String cmd = mComBCommandEt.getText().toString();
            if (!TextUtils.isEmpty(cmd)) {
                if (isComBAutoSend) {
                    if (isComBSendHex) {
                        if (!Utils.isHexString(cmd)) {
                            Toast.makeText(this, R.string.invalid_hex_string, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (isComBLineBreak) {
                            mComBHelper.setHexLoopData(cmd + String.valueOf((int) ('\r')) + String.valueOf((int) ('\n')));
                        } else {
                            mComBHelper.setHexLoopData(cmd);
                        }
                    } else {
                        if (isComBLineBreak) {
                            mComBHelper.setTxtLoopData(cmd + "\r\n");
                        } else {
                            mComBHelper.setTxtLoopData(cmd);
                        }
                    }
                    try {
                        int delayTime = Integer.parseInt(mComBAutoSendDelayedTimeEt.getText().toString());
                        mComBHelper.setExceDelay(delayTime);
                    } catch (Exception e) {
                        Log.e(TAG, "comBSendCommand=>error: ", e);
                    }
                    mComBHelper.startSend();
                } else {
                    if (isComBSendHex) {
                        if (!Utils.isHexString(cmd)) {
                            Toast.makeText(this, R.string.invalid_hex_string, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (isComBLineBreak) {
                            mComBHelper.sendHex(cmd + String.valueOf((int) ('\r')) + String.valueOf((int) ('\n')));
                        } else {
                            mComBHelper.sendHex(cmd);
                        }
                    } else {
                        if (isComBLineBreak) {
                            mComBHelper.sendTxt(cmd + "\r\n");
                        } else {
                            mComBHelper.sendTxt(cmd);
                        }
                    }
                    if (mSerialPorts > 1) {
                        cmd = String.format("%6s[%-7s]: %s", "[COMB]", getString(R.string.send_tag), cmd);
                    } else {
                        cmd = String.format("[%-7s]: %s", getString(R.string.send_tag), cmd);
                    }
                    mDispThread.addLogToQueue(cmd);
                }
            } else {
                Toast.makeText(this, R.string.command_not_allow_empty, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void comCSendCommand() {
        if (mComCHelper.isOpen()) {
            String cmd = mComCCommandEt.getText().toString();
            if (!TextUtils.isEmpty(cmd)) {
                if (isComCAutoSend) {
                    if (isComCSendHex) {
                        if (!Utils.isHexString(cmd)) {
                            Toast.makeText(this, R.string.invalid_hex_string, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (isComCLineBreak) {
                            mComCHelper.setHexLoopData(cmd + String.valueOf((int) ('\r')) + String.valueOf((int) ('\n')));
                        } else {
                            mComCHelper.setHexLoopData(cmd);
                        }
                    } else {
                        if (isComCLineBreak) {
                            mComCHelper.setTxtLoopData(cmd + "\r\n");
                        } else {
                            mComCHelper.setTxtLoopData(cmd);
                        }
                    }
                    try {
                        int delayTime = Integer.parseInt(mComCAutoSendDelayedTimeEt.getText().toString());
                        mComCHelper.setExceDelay(delayTime);
                    } catch (Exception e) {
                        Log.e(TAG, "comCSendCommand=>error: ", e);
                    }
                    mComCHelper.startSend();
                } else {
                    if (isComCSendHex) {
                        if (!Utils.isHexString(cmd)) {
                            Toast.makeText(this, R.string.invalid_hex_string, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (isComCLineBreak) {
                            mComCHelper.sendHex(cmd + String.valueOf((int) ('\r')) + String.valueOf((int) ('\n')));
                        } else {
                            mComCHelper.sendHex(cmd);
                        }
                    } else {
                        if (isComCLineBreak) {
                            mComCHelper.sendTxt(cmd + "\r\n");
                        } else {
                            mComCHelper.sendTxt(cmd);
                        }
                    }
                    if (mSerialPorts > 1) {
                        cmd = String.format("%6s[%-7s]: %s", "[COMC]", getString(R.string.send_tag), cmd);
                    } else {
                        cmd = String.format("[%-7s]: %s", getString(R.string.send_tag), cmd);
                    }
                    mDispThread.addLogToQueue(cmd);
                }
            } else {
                Toast.makeText(this, R.string.command_not_allow_empty, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void comDSendCommand() {
        if (mComDHelper.isOpen()) {
            String cmd = mComDCommandEt.getText().toString();
            if (!TextUtils.isEmpty(cmd)) {
                if (isComDAutoSend) {
                    if (isComDSendHex) {
                        if (!Utils.isHexString(cmd)) {
                            Toast.makeText(this, R.string.invalid_hex_string, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (isComDLineBreak) {
                            mComDHelper.setHexLoopData(cmd + String.valueOf((int) ('\r')) + String.valueOf((int) ('\n')));
                        } else {
                            mComDHelper.setHexLoopData(cmd);
                        }
                    } else {
                        if (isComDLineBreak) {
                            mComDHelper.setTxtLoopData(cmd + "\r\n");
                        } else {
                            mComDHelper.setTxtLoopData(cmd);
                        }
                    }
                    try {
                        int delayTime = Integer.parseInt(mComDAutoSendDelayedTimeEt.getText().toString());
                        mComDHelper.setExceDelay(delayTime);
                    } catch (Exception e) {
                        Log.e(TAG, "comDSendCommand=>error: ", e);
                    }
                    mComDHelper.startSend();
                } else {
                    if (isComDSendHex) {
                        if (!Utils.isHexString(cmd)) {
                            Toast.makeText(this, R.string.invalid_hex_string, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (isComDLineBreak) {
                            mComDHelper.sendHex(cmd + String.valueOf((int) ('\r')) + String.valueOf((int) ('\n')));
                        } else {
                            mComDHelper.sendHex(cmd);
                        }
                    } else {
                        if (isComDLineBreak) {
                            mComDHelper.sendTxt(cmd + "\r\n");
                        } else {
                            mComDHelper.sendTxt(cmd);
                        }
                    }
                    if (mSerialPorts > 1) {
                        cmd = String.format("%6s[%-7s]: %s", "[COMD]", getString(R.string.send_tag), cmd);
                    } else {
                        cmd = String.format("[%-7s]: %s", getString(R.string.send_tag), cmd);
                    }
                    mDispThread.addLogToQueue(cmd);
                }
            } else {
                Toast.makeText(this, R.string.command_not_allow_empty, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void saveLog() {
        if (mLogEt.length() > 0) {
            if (mEditFileNameDialog == null) {
                initEditFileNameDialog();
            }
            if (!mEditFileNameDialog.isShowing()) {
                mFileNameEt.setHint(mSimpleDateFormat.format(Calendar.getInstance().getTime()));
                mEditFileNameDialog.show();
            }
        } else {
            Toast.makeText(this, R.string.no_log_save_tip, Toast.LENGTH_SHORT).show();
        }
    }

    private boolean saveLogToFile(String fileName) {
        boolean result = false;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            File logFile = new File(getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), fileName + ".txt");
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(logFile);
                fos.write(mLogEt.getText().toString().getBytes());
                fos.flush();
                result = true;
                Toast.makeText(this, getString(R.string.log_file_path, logFile.getAbsoluteFile()), Toast.LENGTH_SHORT).show();
            } catch (FileNotFoundException e) {
                Log.e(TAG, "saveLogToFile=>error: ", e);
            } catch (IOException e) {
                Log.e(TAG, "saveLogToFile=>error: ", e);
            } finally {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {}
                    fos = null;
                }
                logFile = null;
            }
        }
        return result;
    }

    private void log(String msg) {
        if (DEBUG) Log.d(TAG, msg);
    }

    private View.OnClickListener mButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.settings:
                    Intent settings = new Intent(ComAssisent.this, SettingsActivity.class);
                    startActivity(settings);
                    break;

                case R.id.clear:
                    mLogEt.setText("");
                    mLines = 0;
                    break;

                case R.id.custom_send:
                    if (isSendingCustomCommand) {
                        mCustomSendThread.stopSendCommand();
                    } else {
                        isSendingCommad = true;
                        isSendingCustomCommand = true;
                        updateUI();
                        int position = mCommandsSpinner.getSelectedItemPosition();
                        CustomCmd cmd = mCustomCmds.get(position);
                        if (mCustomSendThread != null) {
                            mCustomSendThread.stopSendCommand();
                            mCustomSendThread = null;
                        }
                        mCustomSendThread = new SendSequenceCommandThread(cmd);
                        mCustomSendThread.start();
                    }
                    break;

                case R.id.save_log:
                    saveLog();
                    break;

                case R.id.com_a_send:
                    if (isComASendingCommand) {
                        if (isComAAutoSend) {
                            if (mComAHelper != null) {
                                mComAHelper.stopSend();
                            }
                        }
                        isComASendingCommand = false;
                        if (!isSendingCustomCommand && !isComASendingCommand && !isComBSendingCommand
                                && !isComCSendingCommand && !isComDSendingCommand) {
                            isSendingCommad = false;
                        }
                        updateUI();
                    } else {
                        isSendingCommad = true;
                        isComASendingCommand = true;
                        updateUI();
                        comASendCommand();
                        if (!isComAAutoSend) {
                            isComASendingCommand = false;
                            if (!isSendingCustomCommand && !isComASendingCommand && !isComBSendingCommand
                                    && !isComCSendingCommand && !isComDSendingCommand) {
                                isSendingCommad = false;
                            }
                            updateUI();
                        }
                    }
                    break;

                case R.id.com_b_send:
                    if (isComBSendingCommand) {
                        if (isComBAutoSend) {
                            if (mComBHelper != null) {
                                mComBHelper.stopSend();
                            }
                        }
                        isComBSendingCommand = false;
                        if (!isSendingCustomCommand && !isComASendingCommand && !isComBSendingCommand
                                && !isComCSendingCommand && !isComDSendingCommand) {
                            isSendingCommad = false;
                        }
                        updateUI();
                    } else {
                        isSendingCommad = true;
                        isComBSendingCommand = true;
                        updateUI();
                        comBSendCommand();
                        if (!isComBAutoSend) {
                            isComBSendingCommand = false;
                            if (!isSendingCustomCommand && !isComASendingCommand && !isComBSendingCommand
                                    && !isComCSendingCommand && !isComDSendingCommand) {
                                isSendingCommad = false;
                            }
                            updateUI();
                        }
                    }
                    break;

                case R.id.com_c_send:
                    if (isComCSendingCommand) {
                        if (isComCAutoSend) {
                            if (mComCHelper != null) {
                                mComCHelper.stopSend();
                            }
                        }
                        isComCSendingCommand = false;
                        if (!isSendingCustomCommand && !isComASendingCommand && !isComBSendingCommand
                                && !isComCSendingCommand && !isComDSendingCommand) {
                            isSendingCommad = false;
                        }
                        updateUI();
                    } else {
                        isSendingCommad = true;
                        isComCSendingCommand = true;
                        updateUI();
                        comCSendCommand();
                        if (!isComCAutoSend) {
                            isComCSendingCommand = false;
                            if (!isSendingCustomCommand && !isComASendingCommand && !isComBSendingCommand
                                    && !isComCSendingCommand && !isComDSendingCommand) {
                                isSendingCommad = false;
                            }
                            updateUI();
                        }
                    }
                    break;

                case R.id.com_d_send:
                    if (isComDSendingCommand) {
                        if (isComDAutoSend) {
                            if (mComDHelper != null) {
                                mComDHelper.stopSend();
                            }
                        }
                        isComDSendingCommand = false;
                        if (!isSendingCustomCommand && !isComASendingCommand && !isComBSendingCommand
                                && !isComCSendingCommand && !isComDSendingCommand) {
                            isSendingCommad = false;
                        }
                        updateUI();
                    } else {
                        isSendingCommad = true;
                        isComDSendingCommand = true;
                        updateUI();
                        comDSendCommand();
                        if (!isComDAutoSend) {
                            isComDSendingCommand = false;
                            if (!isSendingCustomCommand && !isComASendingCommand && !isComBSendingCommand
                                    && !isComCSendingCommand && !isComDSendingCommand) {
                                isSendingCommad = false;
                            }
                            updateUI();
                        }
                    }
                    break;
            }
        }
    };

    private CompoundButton.OnCheckedChangeListener mCheckBoxCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (buttonView.getId()) {
                case R.id.auto_clear:
                    log("onCheckedChanged=>auto clear isChecked: " + isChecked);
                    String line = mMaxLineEt.getText().toString();
                    try {
                        mMaxLine = Integer.parseInt(line);
                        isAutoClear = isChecked;
                    } catch (Exception e) {
                        Log.e(TAG, "onCheckedChanged=>auto clear error: ", e);
                        Toast.makeText(ComAssisent.this, R.string.max_line_error, Toast.LENGTH_SHORT).show();
                        isAutoClear = false;
                    }
                    updateUI();
                    break;

                case R.id.com_a_line_break:
                    isComALineBreak = isChecked;
                    break;

                case R.id.com_a_auto_send:
                    isComAAutoSend = isChecked;
                    updateUI();
                    break;

                case R.id.com_b_line_break:
                    isComBLineBreak = isChecked;
                    break;

                case R.id.com_b_auto_send:
                    isComBAutoSend = isChecked;
                    updateUI();
                    break;

                case R.id.com_c_line_break:
                    isComCLineBreak = isChecked;
                    break;

                case R.id.com_c_auto_send:
                    isComCAutoSend = isChecked;
                    updateUI();
                    break;

                case R.id.com_d_line_break:
                    isComDLineBreak = isChecked;
                    break;

                case R.id.com_d_auto_send:
                    isComDAutoSend = isChecked;
                    updateUI();
                    break;
            }

        }
    };

    private CompoundButton.OnCheckedChangeListener mRadioButtonCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (buttonView.getId()) {
                case R.id.com_a_txt:
                    isComASendHex = !isChecked;
                    break;

                case R.id.com_a_hex:
                    isComASendHex = isChecked;
                    break;

                case R.id.com_b_txt:
                    isComBSendHex = !isChecked;
                    break;

                case R.id.com_b_hex:
                    isComBSendHex = isChecked;
                    break;

                case R.id.com_c_txt:
                    isComCSendHex = !isChecked;
                    break;

                case R.id.com_c_hex:
                    isComCSendHex = isChecked;
                    break;

                case R.id.com_d_txt:
                    isComDSendHex = !isChecked;
                    break;

                case R.id.com_d_hex:
                    isComDSendHex = isChecked;
                    break;
            }
        }
    };

    private ToggleButton.OnCheckedChangeListener mToggleButtonCheckedChangeListener = new ToggleButton.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            String port = null;
            switch (buttonView.getId()) {
                case R.id.com_a_toggle:
                    port = (String) mComASerialPortSpinner.getSelectedItem();
                    if (isChecked) {
                        if (getSerialPortOpen(port) == null) {
                            String rate = (String) mComABaudRateSpinner.getSelectedItem();
                            int baudRate = Integer.parseInt(rate);
                            mComAHelper = new SerialPortHelper(ComAssisent.this, "COMA", port, baudRate, mComADataReceiver);
                            try {
                                mComAHelper.open();
                            } catch (Exception e) {
                                Log.e(TAG, "onCheckedChanged=>error: ", e);
                                Toast.makeText(ComAssisent.this, R.string.open_serial_port_fail, Toast.LENGTH_SHORT).show();
                                mComAHelper.close();
                                mComAHelper = null;
                            }
                        } else {
                            Toast.makeText(ComAssisent.this, R.string.serial_port_already_opened, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (mComAHelper != null) {
                            mComAHelper.close();
                            mComAHelper = null;
                        }
                        isComASendingCommand = false;
                        if (!isSendingCustomCommand && !isComASendingCommand && !isComBSendingCommand
                                && !isComCSendingCommand && !isComDSendingCommand) {
                            isSendingCommad = false;
                        }
                    }
                    updateUI();
                    break;

                case R.id.com_b_toggle:
                    port = (String) mComBSerialPortSpinner.getSelectedItem();
                    if (isChecked) {
                        if (getSerialPortOpen(port) == null) {
                            String rate = (String) mComBBaudRateSpinner.getSelectedItem();
                            int baudRate = Integer.parseInt(rate);
                            mComBHelper = new SerialPortHelper(ComAssisent.this, "COMB", port, baudRate, mComBDataReceiver);
                            try {
                                mComBHelper.open();
                            } catch (Exception e) {
                                Log.e(TAG, "onCheckedChanged=>error: ", e);
                                Toast.makeText(ComAssisent.this, R.string.open_serial_port_fail, Toast.LENGTH_SHORT).show();
                                mComBHelper.close();
                                mComBHelper = null;
                            }
                        } else {
                            Toast.makeText(ComAssisent.this, R.string.serial_port_already_opened, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (mComBHelper != null) {
                            mComBHelper.close();
                            mComBHelper = null;
                        }
                        isComBSendingCommand = false;
                        if (!isSendingCustomCommand && !isComASendingCommand && !isComBSendingCommand
                                && !isComCSendingCommand && !isComDSendingCommand) {
                            isSendingCommad = false;
                        }
                    }
                    updateUI();
                    break;

                case R.id.com_c_toggle:
                    port = (String) mComCSerialPortSpinner.getSelectedItem();
                    if (isChecked) {
                        if (getSerialPortOpen(port) == null) {
                            String rate = (String) mComCBaudRateSpinner.getSelectedItem();
                            int baudRate = Integer.parseInt(rate);
                            mComCHelper = new SerialPortHelper(ComAssisent.this, "COMC", port, baudRate, mComCDataReceiver);
                            try {
                                mComCHelper.open();
                            } catch (Exception e) {
                                Log.e(TAG, "onCheckedChanged=>error: ", e);
                                Toast.makeText(ComAssisent.this, R.string.open_serial_port_fail, Toast.LENGTH_SHORT).show();
                                mComCHelper.close();
                                mComCHelper = null;
                            }
                        } else {
                            Toast.makeText(ComAssisent.this, R.string.serial_port_already_opened, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (mComCHelper != null) {
                            mComCHelper.close();
                            mComCHelper = null;
                        }
                        isComCSendingCommand = false;
                        if (!isSendingCustomCommand && !isComASendingCommand && !isComBSendingCommand
                                && !isComCSendingCommand && !isComDSendingCommand) {
                            isSendingCommad = false;
                        }
                    }
                    updateUI();
                    break;

                case R.id.com_d_toggle:
                    port = (String) mComDSerialPortSpinner.getSelectedItem();
                    if (isChecked) {
                        if (getSerialPortOpen(port) == null) {
                            String rate = (String) mComDBaudRateSpinner.getSelectedItem();
                            int baudRate = Integer.parseInt(rate);
                            mComDHelper = new SerialPortHelper(ComAssisent.this, "COMD", port, baudRate, mComDDataReceiver);
                            try {
                                mComDHelper.open();
                            } catch (Exception e) {
                                Log.e(TAG, "onCheckedChanged=>error: ", e);
                                Toast.makeText(ComAssisent.this, R.string.open_serial_port_fail, Toast.LENGTH_SHORT).show();
                                mComDHelper.close();
                                mComDHelper = null;
                            }
                        } else {
                            Toast.makeText(ComAssisent.this, R.string.serial_port_already_opened, Toast.LENGTH_SHORT).show();
                            mComDToggleTb.setChecked(false);
                        }
                    } else {
                        if (mComDHelper != null) {
                            mComDHelper.close();
                            mComDHelper = null;
                        }
                        isComDSendingCommand = false;
                        if (!isSendingCustomCommand && !isComASendingCommand && !isComBSendingCommand
                                && !isComCSendingCommand && !isComDSendingCommand) {
                            isSendingCommad = false;
                        }
                    }
                    updateUI();
                    break;
            }
        }
    };

    private SerialPortHelper.OnDataReceive mComADataReceiver = new SerialPortHelper.OnDataReceive() {
        @Override
        public void onDataReceive(byte[] data, int length) {
            byte[] realData = new byte[length];
            realData = Arrays.copyOfRange(data, 0, length);
            String log = new String(realData);
            if (isComASendHex) {
                String hexLog = Utils.ByteArrToHex(realData);
                if (isConverHexLogToString) {
                    hexLog = log;
                }
                Log.d(TAG, "onDataReceive=>log: " + hexLog);
                if (mSerialPorts > 1 || isSendingCustomCommand) {
                    hexLog = String.format("%6s[%-7s]: %s", "[COMA]", getString(R.string.receive_tag), hexLog);
                } else {
                    hexLog = String.format("[%-7s]: %s", getString(R.string.receive_tag), hexLog);
                }
                mDispThread.addLogToQueue(hexLog);
            } else {
                Log.d(TAG, "onDataReceive=>log: " + log);
                boolean isLastLineBreak = (log.charAt(log.length() - 2) == '\r' && log.charAt(log.length() - 1) == '\n');
                log = mComAIncompleteLog + log;
                log.trim();
                String[] logs = log.split("\r\n");
                int len = logs.length;
                if (!isLastLineBreak) {
                    len--;
                    mComAIncompleteLog = logs[logs.length - 1];
                } else {
                    mComAIncompleteLog = "";
                }
                for (int i = 0; i < len; i++) {
                    if (!TextUtils.isEmpty(logs[i])) {
                        if (mSerialPorts > 1 || isSendingCustomCommand) {
                            log = String.format("%6s[%-7s]: %s", "[COMA]", getString(R.string.receive_tag), logs[i]);
                        } else {
                            log = String.format("[%-7s]: %s", getString(R.string.receive_tag), logs[i]);
                        }
                        mDispThread.addLogToQueue(log);
                    }
                }
            }
        }

        @Override
        public void onSendCommand(byte[] command) {
            String cmd = new String(command).trim();
            if (isComASendHex) {
                cmd = Utils.ByteArrToHex(command);
                if (isComALineBreak) {
                    cmd = Utils.ByteArrToHex(command, 0, command.length - 2);
                }
            }
            if (mSerialPorts > 1 || isSendingCustomCommand) {
                cmd = String.format("%6s[%-7s]: %s", "[COMA]", getString(R.string.send_tag), cmd);
            } else {
                cmd = String.format("[%-7s]: %s", getString(R.string.send_tag), cmd);
            }
            mDispThread.addLogToQueue(cmd);
        }
    };

    private SerialPortHelper.OnDataReceive mComBDataReceiver = new SerialPortHelper.OnDataReceive() {
        @Override
        public void onDataReceive(byte[] data, int length) {
            byte[] realData = new byte[length];
            realData = Arrays.copyOfRange(data, 0, length);
            String log = new String(realData);
            if (isComBSendHex) {
                String hexLog = Utils.ByteArrToHex(realData);
                if (isConverHexLogToString) {
                    hexLog = log;
                }
                Log.d(TAG, "onDataReceive=>log: " + hexLog);
                if (mSerialPorts > 1 || isSendingCustomCommand) {
                    hexLog = String.format("%6s[%-7s]: %s", "[COMB]", getString(R.string.receive_tag), hexLog);
                } else {
                    hexLog = String.format("[%-7s]: %s", "[COMB]", getString(R.string.receive_tag), hexLog);
                }
                mDispThread.addLogToQueue(hexLog);
            } else {
                Log.d(TAG, "onDataReceive=>log: " + log);
                boolean isLastLineBreak = (log.charAt(log.length() - 2) == '\r' && log.charAt(log.length() - 1) == '\n');
                log = mComBIncompleteLog + log;
                log.trim();
                String[] logs = log.split("\r\n");
                int len = logs.length;
                if (!isLastLineBreak) {
                    len--;
                    mComBIncompleteLog = logs[logs.length - 1];
                } else {
                    mComBIncompleteLog = "";
                }
                for (int i = 0; i < len; i++) {
                    if (!TextUtils.isEmpty(logs[i])) {
                        if (mSerialPorts > 1 || isSendingCustomCommand) {
                            log = String.format("%6s[%-7s]: %s", "[COMB]", getString(R.string.receive_tag), logs[i]);
                        } else {
                            log = String.format("[%-7s]: %s", getString(R.string.receive_tag), logs[i]);
                        }
                        mDispThread.addLogToQueue(log);
                    }
                }
            }
        }

        @Override
        public void onSendCommand(byte[] command) {
            String cmd = new String(command).trim();
            if (isComBSendHex) {
                cmd = Utils.ByteArrToHex(command);
                if (isComBLineBreak) {
                    cmd = Utils.ByteArrToHex(command, 0, command.length - 2);
                }
            }
            if (mSerialPorts > 1 || isSendingCustomCommand) {
                cmd = String.format("%6s[%-7s]: %s", "[COMB]", getString(R.string.send_tag), cmd);
            } else {
                cmd = String.format("[%-7s]: %s", getString(R.string.send_tag), cmd);
            }
            mDispThread.addLogToQueue(cmd);
        }
    };

    private SerialPortHelper.OnDataReceive mComCDataReceiver = new SerialPortHelper.OnDataReceive() {
        @Override
        public void onDataReceive(byte[] data, int length) {
            byte[] realData = new byte[length];
            realData = Arrays.copyOfRange(data, 0, length);
            String log = new String(realData);
            if (isComCSendHex) {
                String hexLog = Utils.ByteArrToHex(realData);
                if (isConverHexLogToString) {
                    hexLog = log;
                }
                Log.d(TAG, "onDataReceive=>log: " + hexLog);
                if (mSerialPorts > 1 || isSendingCustomCommand) {
                    hexLog = String.format("%6s[%-7s]: %s", "[COMC]", getString(R.string.receive_tag), hexLog);
                } else {
                    hexLog = String.format("[%-7s]: %s", getString(R.string.receive_tag), hexLog);
                }
                mDispThread.addLogToQueue(hexLog);
            } else {
                Log.d(TAG, "onDataReceive=>log: " + log);
                boolean isLastLineBreak = (log.charAt(log.length() - 2) == '\r' && log.charAt(log.length() - 1) == '\n');
                log = mComCIncompleteLog + log;
                log.trim();
                String[] logs = log.split("\r\n");
                int len = logs.length;
                if (!isLastLineBreak) {
                    len--;
                    mComCIncompleteLog = logs[logs.length - 1];
                } else {
                    mComCIncompleteLog = "";
                }
                for (int i = 0; i < len; i++) {
                    if (!TextUtils.isEmpty(logs[i])) {
                        if (mSerialPorts > 1 || isSendingCustomCommand) {
                            log = String.format("%6s[%-7s]: %s", "[COMC]", getString(R.string.receive_tag), logs[i]);
                        } else {
                            log = String.format("[%-7s]: %s", getString(R.string.receive_tag), logs[i]);
                        }
                        mDispThread.addLogToQueue(log);
                    }
                }
            }
        }

        @Override
        public void onSendCommand(byte[] command) {
            String cmd = new String(command).trim();
            if (isComCSendHex) {
                cmd = Utils.ByteArrToHex(command);
                if (isComCLineBreak) {
                    cmd = Utils.ByteArrToHex(command, 0, command.length - 2);
                }
            }
            if (mSerialPorts > 1 || isSendingCustomCommand) {
                cmd = String.format("%6s[%-7s]: %s", "[COMC]", getString(R.string.send_tag), cmd);
            } else {
                cmd = String.format("[%-7s]: %s", getString(R.string.send_tag), cmd);
            }
            mDispThread.addLogToQueue(cmd);
        }
    };

    private SerialPortHelper.OnDataReceive mComDDataReceiver = new SerialPortHelper.OnDataReceive() {
        @Override
        public void onDataReceive(byte[] data, int length) {
            byte[] realData = new byte[length];
            realData = Arrays.copyOfRange(data, 0, length);
            String log = new String(realData);
            if (isComDSendHex) {
                String hexLog = Utils.ByteArrToHex(realData);
                if (isConverHexLogToString) {
                    hexLog = log;
                }
                Log.d(TAG, "onDataReceive=>log: " + hexLog);
                if (mSerialPorts > 1 || isSendingCustomCommand) {
                    hexLog = String.format("%6s[%-7s]: %s", "[COMD]", getString(R.string.receive_tag), hexLog);
                } else {
                    hexLog = String.format("[%-7s]: %s", getString(R.string.receive_tag), hexLog);
                }
                mDispThread.addLogToQueue(hexLog);
            } else {
                Log.d(TAG, "onDataReceive=>log: " + log);
                boolean isLastLineBreak = (log.charAt(log.length() - 2) == '\r' && log.charAt(log.length() - 1) == '\n');
                log = mComDIncompleteLog + log;
                log.trim();
                String[] logs = log.split("\r\n");
                int len = logs.length;
                if (!isLastLineBreak) {
                    len--;
                    mComDIncompleteLog = logs[logs.length - 1];
                } else {
                    mComDIncompleteLog = "";
                }
                for (int i = 0; i < len; i++) {
                    if (!TextUtils.isEmpty(logs[i])) {
                        if (mSerialPorts > 1 || isSendingCustomCommand) {
                            log = String.format("%6s[%-7s]: %s", "[COMD]", getString(R.string.receive_tag), logs[i]);
                        } else {
                            log = String.format("[%-7s]: %s", getString(R.string.receive_tag), logs[i]);
                        }
                        mDispThread.addLogToQueue(log);
                    }
                }
            }
        }

        @Override
        public void onSendCommand(byte[] command) {
            String cmd = new String(command).trim();
            if (isComDSendHex) {
                cmd = Utils.ByteArrToHex(command);
                if (isComDLineBreak) {
                    cmd = Utils.ByteArrToHex(command, 0, command.length - 2);
                }
            }
            if (mSerialPorts > 1 || isSendingCustomCommand) {
                cmd = String.format("%6s[%-7s]: %s", "[COMD]", getString(R.string.send_tag), cmd);
            } else {
                cmd = String.format("[%-7s]: %s", getString(R.string.send_tag), cmd);
            }
            mDispThread.addLogToQueue(cmd);
        }
    };

    private SerialPortHelper.OnDataReceive mCommandsDataReceiver = new SerialPortHelper.OnDataReceive() {
        @Override
        public void onDataReceive(byte[] data, int length) {
            byte[] realData = new byte[length];
            realData = Arrays.copyOfRange(data, 0, length);
            String log = new String(realData);
            if (mCustomSendThread != null) {
                Command cmd = mCustomSendThread.getCurrentCommand();
                if (cmd != null) {
                    if (cmd.isSendHex) {
                        String hexLog = Utils.ByteArrToHex(realData);
                        if (isConverHexLogToString) {
                            hexLog = log;
                        }
                        Log.d(TAG, "onDataReceive=>log: " + hexLog);
                        hexLog = String.format("%6s[%-7s]: %s", "[CUST]", getString(R.string.receive_tag), hexLog);
                        mDispThread.addLogToQueue(hexLog);
                    } else {
                        Log.d(TAG, "onDataReceive=>log: " + log);
                        boolean isLastLineBreak = (log.charAt(log.length() - 2) == '\r' && log.charAt(log.length() - 1) == '\n');
                        log = mCustomIncompleteLog + log;
                        log.trim();
                        String[] logs = log.split("\r\n");
                        int len = logs.length;
                        if (!isLastLineBreak) {
                            len--;
                            mCustomIncompleteLog = logs[logs.length - 1];
                        } else {
                            mCustomIncompleteLog = "";
                        }
                        for (int i = 0; i < len; i++) {
                            if (!TextUtils.isEmpty(logs[i])) {
                                log = String.format("%6s[%-7s]: %s", "[CUST]", getString(R.string.receive_tag), logs[i]);
                                mDispThread.addLogToQueue(log);
                            }
                        }
                    }
                } else {
                    Log.e(TAG, "onDataReceive=>custom command is null.");
                }
            } else {
                Log.e(TAG, "onDataReceive=>custom send thread ended.");
            }
        }

        @Override
        public void onSendCommand(byte[] command) {
        }
    };

    /**
     * 刷新显示线程
     */
    private class DispQueueThread extends Thread {

        private boolean isStop;

        private Queue<String> mQueueList = new LinkedList<String>();

        @Override
        public void run() {
            while (!isInterrupted()) {
                final String log;
                while ((log = mQueueList.poll()) != null && !isStop) {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            if (mLines >= mMaxLine) {
                                if (isAutoClear) {
                                    mLogEt.setText("");
                                }
                                mLines = 0;
                            }
                            mLogEt.append(log + "\n");
                            mLines++;
                        }
                    });
                    try {
                        Thread.sleep(100);//显示性能高的话，可以把此数值调小。
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
        }

        public synchronized void addLogToQueue(String log) {
            mQueueList.add(log);
        }

        public synchronized void stopThread() {
            isStop = true;
            notify();
        }
    }

    private class SendSequenceCommandThread extends Thread {

        private SerialPortHelper mCustomHelper;
        private ArrayList<Command> mCommands;

        private String mSerialPort;
        private int mBaudRate;
        private int mIndex;
        private boolean isRepeat;
        private boolean isStop;

        public SendSequenceCommandThread(CustomCmd cmd) {
            mCommands = cmd.commands;
            mSerialPort = cmd.serialPort;
            mBaudRate = cmd.baudRate;
            mIndex = 0;
            isRepeat = cmd.isRepeat;
            isStop = false;
        }

        public Command getCurrentCommand() {
            if (mIndex > 0) {
                return mCommands.get((mIndex - 1) % mCommands.size());
            } else {
                return null;
            }
        }

        public SerialPortHelper getCustomHelper() {
            return mCustomHelper;
        }

        public void stopSendCommand() {
            try {
                isStop = true;
                interrupt();
            } catch (Exception e) {
                Log.e(TAG, "stopSendCommand=>error: ", e);
            }
        }

        public boolean isStop() {
            return isStop;
        }

        @Override
        public void run() {
            boolean isCustomHelper = false;
            mCustomHelper = getSerialPortOpen(mSerialPort);
            if (mCustomHelper == null) {
                mCustomHelper = new SerialPortHelper(ComAssisent.this, "CUST", mSerialPort, mBaudRate, mCommandsDataReceiver);
                isCustomHelper = true;
            } else {
                if (!mSerialPort.equals(mCustomHelper.getPort()) || (mBaudRate != mCustomHelper.getBaudRate())) {
                    showToast(R.string.serial_port_already_opened);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            isSendingCustomCommand = false;
                            if (!isSendingCustomCommand && !isComASendingCommand && !isComBSendingCommand
                                    && !isComCSendingCommand && !isComDSendingCommand) {
                                isSendingCommad = false;
                            }
                            updateUI();
                        }
                    });
                    mCustomHelper = null;
                    Log.d(TAG, "SendSequenceCommandThread=>thread is end.");
                    return;
                }
            }
            try {
                mCustomHelper.open();
                Command cmd = null;
                while (!isStop && (((mIndex < mCommands.size()) && !isRepeat) || isRepeat)) {
                    cmd = mCommands.get(mIndex % mCommands.size());
                    Log.d(TAG, "run=>send command: " + cmd);
                    if (cmd.isSendHex) {
                        if (!Utils.isHexString(cmd.cmd)) {
                            showToast(R.string.invalid_hex_string);
                            return;
                        }
                        if (cmd.isAddLineBreak) {
                            mCustomHelper.sendHex(cmd.cmd + String.valueOf((int) ('\r')) + String.valueOf((int) ('\n')));
                        } else {
                            mCustomHelper.sendHex(cmd.cmd);
                        }
                    } else {
                        if (cmd.isAddLineBreak) {
                            mCustomHelper.sendTxt(cmd.cmd + "\r\n");
                        } else {
                            mCustomHelper.sendTxt(cmd.cmd);
                        }
                    }
                    String log = String.format("%6s[%-7s]: %s", "[CUST]", getString(R.string.send_tag), cmd.cmd);
                    mDispThread.addLogToQueue(log);
                    mIndex++;

                    try {
                        sleep(cmd.exceTime);
                    } catch (InterruptedException e) {
                        Log.e(TAG, "run=>error: ", e);
                    }
                }
                isStop = true;
            } catch (Exception e) {
                Log.e(TAG, "SendSequenceCommandThread=>run error: ", e);
                showToast(R.string.open_serial_port_fail);
            }

            if (mCustomHelper != null && isCustomHelper) {
                mCustomHelper.close();
            }
            mCustomHelper = null;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    isSendingCustomCommand = false;
                    if (!isSendingCustomCommand && !isComASendingCommand && !isComBSendingCommand
                            && !isComCSendingCommand && !isComDSendingCommand) {
                        isSendingCommad = false;
                    }
                    updateUI();
                }
            });
            Log.d(TAG, "SendSequenceCommandThread=>thread is end.");
        }

        private void showToast(final String msg) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(ComAssisent.this, msg, Toast.LENGTH_SHORT).show();
                }
            });
        }

        private void showToast(final int msgId) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(ComAssisent.this, getString(msgId), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
